const functions = require("firebase-functions");
const { MongoClient } = require('mongodb');
const cors = require('cors')({
  origin: true,
});

const url = 'mongodb://root:8cwJ2ghmSRtzkfQ5pP@172.255.248.122:27017';
const client = new MongoClient(url);

const authItems = [
  {email: "telegram.user456@smertvorogam.in.ua", password: "NDOpenTenSPING"},
  {email: "telegram.user8758567@smertvorogam.in.ua", password: "OcoMaDrETiNtOm"},
  {email: "telegram.user435537@smertvorogam.in.ua", password: "ipAroMpTIdUSaW"},
  {email: "telegram.user08321@smertvorogam.in.ua", password: "PReScrYlIsteRg"},
  {email: "telegram.user9456534@smertvorogam.in.ua", password: "NvelIGinveDEsC"},
  {email: "telegram.user146@smertvorogam.in.ua", password: "NAMiTYPeAvIleD"},
  {email: "telegram.user9234220@smertvorogam.in.ua", password: "ElMaLionkANgfe"},
  {email: "telegram.user73261085@smertvorogam.in.ua", password: "IFINseCkShlogy"},
  {email: "telegram.user5436@smertvorogam.in.ua", password: "AWlaTOiniceStU"},
  {email: "telegram.user892360@smertvorogam.in.ua", password: "IsTeMIntOrmiDa"},
  {email: "telegram.user430291@smertvorogam.in.ua", password: "ynhIOraSHumION"},
  {email: "telegram.user509763984@smertvorogam.in.ua", password: "mitiErSelIxtAT"},
  {email: "telegram.user3401@smertvorogam.in.ua", password: "NTIaNDrYDROSiD"},
];

exports.search = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    const telegramId = req.body.telegramId

    await client.connect();
    const db = client.db('tlgrm');
    const collection = db.collection('data');

    const result = await collection.findOne({
      uid: telegramId
    }, {});
    if (result) {
      delete result._id
      result.systemNumber = result['Системный номер']
    }

    res.json({
      item: result
    })

    await client.close();
  });
});

exports.login = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    const email = req.body.email;
    const password = req.body.password;
    const item = authItems
        .find((item) => item.email === email && item.password === password);
    res.json({
      success: !!item,
    });
  });
});

exports.test = functions.https.onRequest((req, res) => {
  res.send({
    version: '1.0.0'
  });
});
