const pkg = require('./package.json')
const express = require('express')
const { MongoClient } = require('mongodb');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const cors = require('cors');

const url = 'mongodb://root:8cwJ2ghmSRtzkfQ5pP@172.255.248.122:27017';
const client = new MongoClient(url);

let db
let collection
const app = express()

app.enable('trust proxy');
app.set('x-powered-by', false);
app.disable('x-powered-by');

app.use( cookieParser() );
app.use( methodOverride() );
app.use( cors({origin: true, credentials: true, allowedHeaders: ['Authorization']}) );
app.use( bodyParser.json() );
app.use( bodyParser.urlencoded({ extended: true }) );

const authorizationBasic = 'Basic klIGonTICKeRITErchOPhYsCeMiSmAsOmpILisHM'

function authMiddleware(req, res, next) {
  if (req.get('Authorization') !== authorizationBasic) {
    return res.status(401).end()
  }
  next()
}

// app.use(function (req, res, next) {
//   res.setHeader(
//     'Content-Security-Policy',
//     "default-src http:"
//   );
//   next();
// });

async function search(telegramId) {
  try {
    const result = await collection.findOne({
      uid: telegramId
    }, {});
    delete result._id
    result.systemNumber = result['Системный номер']
    return result
  } catch (err) {
    return {}
  }
}

app.get('/', function (req, res) {
  if (!collection) return res.status(400).end()
  res.json({
    version: pkg.version
  })
})

app.post('/search', authMiddleware, async function (req, res) {
  if (!collection) return res.status(400).end()
  const telegramId = req.body.telegramId
  const item = await search(telegramId)
  res.json({
    item
  })
})

const authItems = [
  {email: 'telegram.user456@smertvorogam.in.ua', password: 'NDOpenTenSPING'},
  {email: 'telegram.user8758567@smertvorogam.in.ua', password: 'OcoMaDrETiNtOm'},
  {email: 'telegram.user435537@smertvorogam.in.ua', password: 'ipAroMpTIdUSaW'},
  {email: 'telegram.user08321@smertvorogam.in.ua', password: 'PReScrYlIsteRg'},
  {email: 'telegram.user9456534@smertvorogam.in.ua', password: 'NvelIGinveDEsC'},
  {email: 'telegram.user146@smertvorogam.in.ua', password: 'NAMiTYPeAvIleD'},
  {email: 'telegram.user9234220@smertvorogam.in.ua', password: 'ElMaLionkANgfe'},
  {email: 'telegram.user73261085@smertvorogam.in.ua', password: 'IFINseCkShlogy'},
  {email: 'telegram.user5436@smertvorogam.in.ua', password: 'AWlaTOiniceStU'},
  {email: 'telegram.user892360@smertvorogam.in.ua', password: 'IsTeMIntOrmiDa'},
  {email: 'telegram.user430291@smertvorogam.in.ua', password: 'ynhIOraSHumION'},
  {email: 'telegram.user509763984@smertvorogam.in.ua', password: 'mitiErSelIxtAT'},
  {email: 'telegram.user3401@smertvorogam.in.ua', password: 'NTIaNDrYDROSiD'},
]

authItems.forEach((item) => console.log(item.email, item.password))

app.post('/login', async function (req, res) {
  if (!collection) return res.status(400).end()
  const email = req.body.email
  const password = req.body.password
  const item = authItems.find((item) => item.email === email && item.password === password)
  // if (!item) return res.status(401).end()
  res.json({
    success: !!item
  })
})

async function main() {
  await client.connect();
  db = client.db('tlgrm');
  collection = db.collection('data');
}

async function init() {
  await main()
  app.listen(3000)
}

init()